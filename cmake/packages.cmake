set(BLREVIVE_VERSION "1.0.0-beta.1" CACHE STRING "version of the blrevive library")

CPMAddPackage(
    NAME BLRevive
    VERSION ${BLREVIVE_VERSION}
    GIT_REPOSITORY https://gitlab.com/blrevive/blrevive
    GIT_TAG v${BLREVIVE_VERSION}
)