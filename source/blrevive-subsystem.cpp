#pragma warning(disable:4244)
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>
#include <SdkHeaders.h>
#include <blrevive-subsystem/blrevive-subsystem.h>

using namespace BLRE;

/**
 * **Example hook callback, remove!**
*/
bool EngineActorTickCallback(UObject* Object, FFrame& Stack, void* const Result)
{
    Log->info("FoxGame.FoxGRI.Timer");
    return true;
}

/**
 * Initialization of the module. 
 * 
 * @remark **Do not change the name or remove the __declspec(dllexport) from this function, otherwise BLRevive will fail to load the module!**
 * 
 * @param blre pointer to BLRevive API
*/
extern "C" __declspec(dllexport) void InitializeModule(BLRevive *blre)
{
    // safe reference to blrevive api
    BLReviveAPI = blre;

    // create a logger instance for this module
    Log = blre->LogFactory->Get("blrevive-subsystem");
    Log->info("Initializing blrevive-subsystem");

    // TODO: replace code below with your custom logic, those are just examples to see how to use the BLRevive API
    blre->FunctionDetour->HookPost("FoxGame.FoxGRI_TDM.Timer", EngineActorTickCallback);
}
